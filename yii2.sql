-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 18 2019 г., 15:08
-- Версия сервера: 5.7.20-log
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sertificates`
--

CREATE TABLE `sertificates` (
  `id` int(10) NOT NULL,
  `name` text NOT NULL,
  `img_url` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sertificates`
--

INSERT INTO `sertificates` (`id`, `name`, `img_url`) VALUES
(12, 'укеуе', '775e380030708837360b1013aed836ea.png'),
(14, 'пвап', 'edff730c1fca4b7e290e08c2d12f3a3e.jpg'),
(15, 'выкук', 'e9c012886a0caa434ea87987328cf239.jpg'),
(16, 'erwrewr', '545377e5f427c7aa39c21499ab87067e.jpg'),
(17, 'апвпа', 'f02e735dc26155d354b9a94e307677f2.jpg'),
(18, 'ssadasd', '9342bee65b1f23dbaf62507b877a5334.jpg'),
(19, 'werwr', '519a39b2d2c6e6e8fe5fc733fa2f4e1d.png'),
(20, 'ervesr', '582d08b2a0d617608aac50c380778358.jpg'),
(21, 'tertert', '67420f4299aa9a22d580b4223060708a.jpg'),
(22, 'fgdgdfgdfg', '83dd9a4ce6380a06cbe164f465a85985.jpg'),
(23, 'ewrwe', '8c0ab812cc61b7ea4cc2d74a47e59eaa.jpg'),
(24, 'rwerwerwer', '209b012d8f4881383af82881f35383df.jpg'),
(25, 'rwerwerwerwer', '757991f2a56a80fa9019eba5ec4fe8fa.png');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', '$2y$13$H6kkxrsLDRxAcuIqoTvN8OaIa3BJpDj/xyHNLLvoXqMohGchojUoe', 'admin'),
(2, 'user', '$2y$13$K0pop2Dv0GQ3xD0xZppAPuhfRr0vdpQrxKFszSkjIqkKSeDSWeYAK', 'user');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `sertificates`
--
ALTER TABLE `sertificates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `sertificates`
--
ALTER TABLE `sertificates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
