<?php
//namespace app\controllers;
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
     //   'brandLabel' => Yii::$app->name,
      // 'brandUrl' => Yii::$app->homeUrl,
      // 'options' => [
        //    'class' => 'navbar-inverse navbar-fixed-top',
        //],
   ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
           // ['label' => 'Главная', 'url' => ['/sertificate/index']],  
		//	['label' => 'Админская', 'url' => ['/admin/sertificates/index']],
Yii::$app->user->isGuest ? (
                ['label' => 'Вход', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Выход (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?//= Breadcrumbs::widget([
		  //  'homeLink' => ['label' => 'Главная', 'url' => '/index'],
         //   'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
       // ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<!--<footer class="footer">
    <div class="container">
	<a href="<?=  yii\helpers\Url::to(['/admin/sertificates/index'])?>">Админка</a>
<br>
<?php if(!Yii::$app->user->isGuest): ?>
    <a href="<?=  yii\helpers\Url::to(['/site/logout'])?>"><?=Yii::$app->user->identity['username']?> (Выход)</a>
<?php else: ?>
    <a href="<?=  yii\helpers\Url::to(['/site/login'])?>">Вход</a>
    <br>
    <a href="<?=  yii\helpers\Url::to(['/site/signup'])?>">Регистрация</a>
	<br>
<?php endif; ?>
<?php //debug(Yii::$app->user->identity) ?>
       <p class="pull-left">&copy; Мои сертификаты 2010 -
		<?= date('Y')?></p>

        <p class="pull-right"><?=  Yii::powered() ?></p>
    </div> 
</footer>-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
