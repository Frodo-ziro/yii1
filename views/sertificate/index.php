<div class="grid"><? 
/** @var array $sers */
foreach ($sers as $ser) { ?><div class="grid-item"><div class="pad">
<a data-fancybox="gallery" data-caption="<?= $ser->name ?>"
href="web/uploads/<?= $ser->img_url ?>"><img src="web/uploads/<?= $ser->img_url ?>" alt="image"/></a></div></div>
<?php } ?></div>
<script type="text/javascript">
$('.grid').packery({itemSelector: '.grid-item', gutter: 0});
</script>
<script src="js/plugin.js"></script>
<script src="js/script.js"></script>