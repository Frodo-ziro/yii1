<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
body{
background: url('web/photo.jpeg') no-repeat center center fixed;
overflow-x: hidden;
background-size: cover;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
	}
	
form{
		height: auto;
		width: 450px;
		margin:auto;
		padding: 60px 0px;
		margin-top: 60px;
		background-color:rgba(255,255,255,0.7);
		-webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        -ms-border-radius: 5px;
        -o-border-radius: 5px;
        border-radius: 15px;
        -webkit-box-shadow: 0 0 10px rgba(0,0,0,0.7);
        box-shadow: 0 0 10px rgba(0,0,0,0.7);
        transition: 1s;
	}
	.form-group{
		margin: 20px 0;
	}

@media(max-width: 544px){
	form{
		height: auto;
		width: 100%;
		padding: 40px 0px;
		margin-top: 80px;
	}
}
input[type="text"]{
		width: 80%;
		height: 35px;
		padding: 10px;
		padding-left: 15px;
		height: auto;
		margin:auto;
		color: red;
		box-sizing: border-box;
	}
input[type="password"]{
		width: 80%;
		height: 35px;
		padding: 10px;
		padding-left: 15px;
		height: auto;
		margin:auto;
		color: red;
		box-sizing: border-box;
	}
		
</style>
<div class="container text-center">
<div class="login-form">
    <h1><?= Html::encode($this->title) ?></h1>

    

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
		]); ?>
		
		<div class="form-group">
		<div class="col-xs-12">
        <?= $form->field($model, 'username')->textInput() ?>
		 </div>
		 </div>
		
		<div class="form-group">
		 <div class="col-xs-12">
        <?= $form->field($model, 'password')->passwordInput() ?>
		 </div>
		 </div>
		<div class="form-group">
        <?= $form->field($model, 'rememberMe')->checkbox([]) ?>
		 </div>
        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Вход', ['class' => 'btn btn-primary center', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

   
</div>
</div>
