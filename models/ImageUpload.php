<?php
namespace app\models;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\ImageUpload;



class ImageUpload extends Model
{
    public $img_url;
	public $name;
    public $id;

    public function rules()
    {
        return [
            [['img_url'], 'required'],
            [['img_url'], 'file','extensions'=>'jpg,png']
        ];
    }

    public function uploadFile(UploadedFile $file, $currentImage)
    {

        $this->img_url=$file;
        if($this->validate())
        
        {
            $this->deleteCurrentImage($currentImage);
            return $this->saveImage();
        }
    }

    public function getFolder()
    {
        return Yii::getAlias('@web'). 'uploads/' ;
    }

    public function generateFilename()
    {
        return strtolower(md5(uniqid($this->img_url->baseName)) . '.' . $this->img_url->extension);
    }

    public function deleteCurrentImage($currentImage)
    {
        if($this->fileExists($currentImage))
        {
            unlink($this->getFolder() . $currentImage);
        }
    }

    public function fileExists($currentImage)
   {
       if(!empty($currentImage) && $currentImage != null)
       {
         return file_exists($this->getFolder() . $currentImage); 
       }
   }


    public function saveImage()
    {
        $filename = $this->generateFilename();
        
        $this->img_url->saveAs($this->getFolder() . $filename); 
        
        return $filename;
   }
    
}