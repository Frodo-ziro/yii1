<?php

namespace app\models;
use yii\db\ActiveRecord;
use app\models\ImageUpload;
use yii\web\UploadedFile;



class Sertificates extends ActiveRecord
{
	public static function tableName()
	
	{
		return 'sertificates';
	}
	   
 public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'img_url'], 'string'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img_url' => 'Img Url',
        ];
    }
    
	public function saveImage($filename)
	{
		$this->img_url=$filename;
		return $this->save(false);
	}

	public function getImage()
	{
		return ($this->img_url) ?  '/web/uploads/' . $this->img_url : '/web/uploads/no-image.png';	
	}

	public function deleteImage()
	{
		$imageUploadModel= new ImageUpload();
		$imageUploadModel->deleteCurrentImage($this->img_url);
	}
	public function beforeDelete()
	{
		$this->deleteImage();
		return parent::beforeDelete();
	}

}