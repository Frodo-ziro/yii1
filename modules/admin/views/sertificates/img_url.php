<?php

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\ImageUpload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Sertificates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sertificates-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	
	<?= $form->field($model, 'img_url')->fileInput(['maxlength' => true]) ?>
	
    <div class="form-group">
	
        <?= Html::submitButton('Загрузить', ['class' => 'btn btn-success']) ?>
		
    </div>
	
    <?php ActiveForm::end(); ?>
	
</div>
<script type="text/javascript">
  var fileField = document.getElementById('imageupload-img_url');

  fileField.onchange = function () {

    var file = fileField.files[0];
    var reader  = new FileReader();

    reader.onloadend = function () {

      var img = document.createElement('img');
      img.className = 'preview-image thumbnail';
      img.style.maxWidth = '300px';
	  img.style.maxHeight = '300px';

      var oldImg = document.querySelector('.preview-image');
      if (oldImg) {
        oldImg.remove();
      }


      var formContainer = document.querySelector('.sertificates-form');
      formContainer.appendChild(img);
      img.src = reader.result;

    }

    reader.readAsDataURL(file);
  }

</script>