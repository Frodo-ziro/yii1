<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Sertificates */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sertificates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sertificates-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?= Html::a('Вернуться на главную', ['sertificates/index'], ['class'=>'btn btn-primary']) ?>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Set Image', ['set-image', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name:ntext',
            //'img_url:ntext',
            [
                'label' => 'Изображение',
                'format' => 'html',
                'value' => function ($data) { 
                    return Html::img($data->getImage(),
                     ['width'=>'100px','height'=>'100px']
                    ); 
                   }, 
                  
                ],
        ],
    ]) ?>

</div>
