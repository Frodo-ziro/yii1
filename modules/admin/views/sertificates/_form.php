<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Sertificates;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Sertificates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sertificates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textarea(['rows' => 1])  ?>

    <?= $form->field($model, 'img_url')->textarea(['rows' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>     
    </div>
    <?php

	ActiveForm::end();?>
	
	
	
	
</div>
