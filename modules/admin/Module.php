<?php

namespace app\modules\admin;
use yii\filters\AccessControl;
/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    
     public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action){
                           // return \Yii::$app->user->identity->role === 'admin';
                            if(\Yii::$app->user->identity->role !== 'admin'){
                                return \Yii::$app->getResponse()->redirect('/');
                            }else{
                                return true;
                            }
                        }
                    ],
                ],
            ]
        ];
    } 
	
	/**public function behaviors()
    {
        return [
            'access'    =>  [
                'class' =>  AccessControl::className(),
                'denyCallback'  =>  function($rule, $action)
                {
                    throw new \yii\web\NotFoundHttpException();
                },
                'rules' =>  [
                    [
                        'allow' =>  true,
                        'matchCallback' =>  function($rule, $action)
                        {
                            return \Yii::$app->user->identity->role === 'admin';
                        }
                    ]
                ]
            ]
        ];
    }*/

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}